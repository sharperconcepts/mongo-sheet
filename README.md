# mongo-sheet

Sync a google sheet with a mongo db collection. This will add a column to the sheet data to record the mongo id object it relates to. Records in the spreadsheet without an ID will be assumed to be new records. Prequisits for your google sheet will be that all of the data is verticle and that the first row will contain field names.