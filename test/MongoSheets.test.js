"use strict";

var assert = require('assert');
var {MongoSheets} = require('../src/MongoSheets');

describe('Mongo Sheet', function(){
    this.timeout(7000);
    let config = {
        mongoConnection: 'mongodb://localhost:27017',
        mongoDatabase: 'Sheets101',
        mongoCollection: 'sheetdata',
        googleAPIcred: require('../client_secret.json'),
        googleSheetId: '1lCYucQH7A_wmlu2X_-Oh8PUKbF6Tupkj6SRpz3E9xl8'
    }
    let m = new MongoSheets(config);

    it('Pull Connection Info', (done) => {
        m.connectInfo().then( (info) => {
            assert.equal(info.GoogleSheets.status,"OK","GoogleSheets Fail");
            assert.equal(info.MongoDatabase.status,"OK","Mongo Fail");
            done();
        });
    })

})