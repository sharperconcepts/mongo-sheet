"use strict";

const {GoogleSheets} = require('./lib/GoogleSheets');
const {MongoData} = require('../src/lib/MongoData');

class MongoSheets {

    constructor(config){
        this.MongoData = new MongoData(config.mongoConnection, config.mongoDatabase)
        this.GoogleSheets = new GoogleSheets(config.googleAPIcred, config.googleSheetId)
    }

    async connectInfo(){
        try {
            let sheetResult = await this.GoogleSheets.GetSpreadSheetInfo();
            let mongoResult = await this.MongoData.pullCollections();
            return {
                GoogleSheets: sheetResult,
                MongoDatabase: mongoResult
            }
        } catch (error) {
            throw Error(error);
        }
        
    }

    syncToMongo(){
        console.log("to mongo updated")
    }

    syncToSheets(){
        console.log("to sheets")
    }

}

exports.MongoSheets = MongoSheets;
