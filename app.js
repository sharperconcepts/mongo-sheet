"use strict";
const restify = require('restify');
const {MongoSheets} = require('./src/MongoSheets');
const MongoClient = require('mongodb').MongoClient;

const config = {
  mongoConnection: 'mongodb://127.0.0.1:27017',
  mongoDatabase: 'Sheets101',
  mongoCollection: 'sheetdata',
  googleAPIcred: require('./client_secret.json')
}

function respond(req, res, next) {

  MongoClient.connect(config.mongoConnection, { useNewUrlParser: true }, function(err, client) {
    console.log("Connected successfully to server");
  
    const db = client.db(config.mongoDatabase);
  
    insertDocuments(db, function() {
      client.close();
    });

  });

  var mon = new MongoSheets();
  mon.syncToMongo();
  res.send('olah ' + req.params.name);
  next();
}

const insertDocuments = function(db, callback) {
  // Get the documents collection
  const collection = db.collection(config.mongoCollection);
  // Insert some documents
  collection.insertMany([
    {a : 1}, {a : 2}, {a : 3}
  ], function(err, result) {
    console.log("Inserted 3 documents into the collection");
    callback(result);
  });
}

var server = restify.createServer();
server.get('/hello/:name', respond);
server.head('/hello/:name', respond);

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});



// var GoogleSpreadsheet = require('google-spreadsheet');
// var creds = require('./client_secret.json');
 
// // Create a document object using the ID of the spreadsheet - obtained from its URL.
// var doc = new GoogleSpreadsheet('1lCYucQH7A_wmlu2X_-Oh8PUKbF6Tupkj6SRpz3E9xl8');
 
// // Authenticate with the Google Spreadsheets API.
// doc.useServiceAccountAuth(creds, function (err) {
 
//   // Get all of the rows from the spreadsheet.
//   doc.getRows(1, function (err, rows) {
//     console.log(rows);
//   });
// });