"use strict";

var assert = require('assert');
var {GoogleSheets} = require('../src/lib/GoogleSheets');

const config = {
    mongoConnection: 'mongodb://localhost:27017',
    mongoDatabase: 'Sheets101',
    mongoCollection: 'sheetdata',
    googleAPIcred: require('../client_secret.json'),
    googleSheetId: '1lCYucQH7A_wmlu2X_-Oh8PUKbF6Tupkj6SRpz3E9xl8'
  }

describe('Google Sheets', function() {
    this.timeout(7000);
    let gs = new GoogleSheets(config.googleAPIcred, config.googleSheetId);
    let wide = 0;

    before('Authorize', (done) => {
        gs.authorize().then(
            (auth) => {
                console.log("Authorized");
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    });

    it('Pull Sheet Header', (done) => {
        gs.pullSheetHeader('test-data').then( 
            (headerResponse) => {
                assert.notEqual(headerResponse, null, "Header is NULL");
                assert.equal(headerResponse.status, 'OK', headerResponse.error);
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    });

    it('Pull Sheet Data', (done) => {
        gs.pullSheetData('test-data', 7).then( 
            (sheetData) => {
                assert.notEqual(sheetData, null, "Data is NULL");
                assert.equal(sheetData.status, 'OK', sheetData.error);
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    });

    it('Insert Sheet Data', (done) => {
        const data = [
            ["my-insert-id","Micheal","2","2018-02-19"]
        ];
        gs.insertSheetData('TestData', data).then( 
            (sheetData) => {
                assert.notEqual(sheetData, null, "Data is NULL");
                assert.equal(sheetData.status, 'OK', sheetData.error);
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    });

    it('Update Sheet Data', (done) => {
        const data = [
            ["my-fake-id","John","31","2012-09-24"]
        ];
        gs.updateSheetData('TestData','A6:D6', data).then( 
            (sheetData) => {
                assert.notEqual(sheetData, null, "Data is NULL");
                assert.equal(sheetData.status, 'OK', sheetData.error);
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    });

    it('Get Spreadsheet', (done) => {
        gs.GetSpreadSheetInfo().then( 
            (x) => {
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    });

    it('Insert Column', async () => {
        const ins = await gs.InsertColumn(1233919142,0);
        const ups = await gs.updateSheetData('TestData','A1',[['my-id']]);
    });

});