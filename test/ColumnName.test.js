"use strict";

var assert = require('assert');
var ColumnName = require('../src/lib/ColumnName');

describe('Column Name Utility', function() {

    const col = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".split(",");
    
    it("Number to Column Name A-Z", () => {
        for(let i=0; i<col.length; i++){
            let letter = ColumnName.intToExcelCol(i+1);
            assert.strictEqual(letter,col[i]);
        }
    })

    it("Column Name A-Z to Number", () => {
        for(let i=0; i<col.length; i++){
            let number = ColumnName.excelColToInt(col[i]);
            assert.strictEqual(number,i+1);
        }
    })
    
    it("Number to Column Name AA-AZ", () => {
        for(let i=0; i<col.length; i++){
            let letter = ColumnName.intToExcelCol(i+27);
            assert.strictEqual(letter,"A" + col[i]);
        }
    })

    it("Column Name AA-AZ to Number", () => {
        for(let i=0; i<col.length; i++){
            let number = ColumnName.excelColToInt("A" + col[i]);
            assert.strictEqual(number,i+27);
        }
    })
    

})