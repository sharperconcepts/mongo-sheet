"use strict";
const MongoClient = require('mongodb').MongoClient;

class MongoData {

    constructor(connString,dbName){
        this.dbName = dbName;
        this.connString = connString;
    }

    open(){
        return MongoClient.connect(this.connString);
    }

    pullCollections(){
        return new Promise((resolve, reject) => {
            this.open().then( (client) => {
                this.client = client;
                let db = client.db(this.dbName);
                return db.listCollections().toArray();
            })
            .then( (result) => {
                this.client.close();
                resolve({
                    status: "OK",
                    data: result
                })
            })
            .catch( (err) => {
                this.database.close();
                reject(err)
            })
        });
    }

    saveRecords(collection, data){
        return new Promise((resolve, reject) => {
            this.open().then( (client) => {
                this.client = client;
                let db = client.db(this.dbName);
                return db.collection(collection).insertMany(data);
            })
            .then( (result) => {
                this.client.close();
                resolve({
                    status: "OK"
                })
            })
            .catch( (err) => {
                this.database.close();
                reject(err)
            })
        });
    }

    retreiveRecords(collection, query = {}){
        return new Promise((resolve, reject) => {
            this.open().then( (client) => {
                this.client = client;
                let db = client.db(this.dbName);
                return db.collection(collection).find(query).toArray();
            })
            .then( (result) => {
                this.client.close();
                resolve({
                    status: "OK",
                    data: result
                })
            })
            .catch( (err) => {
                this.database.close();
                reject(err)
            })
        });
    }


}

exports.MongoData = MongoData;