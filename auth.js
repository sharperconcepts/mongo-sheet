
const {JWT} = require('google-auth-library');
var {google} = require("googleapis");
const keys = require('./client_secret.json');

async function main() {
    const client = new JWT(
        keys.client_email,
        null,
        keys.private_key,
        ['https://www.googleapis.com/auth/spreadsheets'],
    );
    await client.authorize();

    const sheets = google.sheets({version: 'v4'});
    sheets.spreadsheets.values.get({
            auth: client,
            spreadsheetId: '1lCYucQH7A_wmlu2X_-Oh8PUKbF6Tupkj6SRpz3E9xl8',
            range: 'Sheet1!A2:C',
        }, 
        (err, response) => {
            if (err) {
                console.log('The API returned an error: ' + err);
                return;
            } 
            var rows = response.data.values;
            if (rows.length === 0) {
                console.log('No data found.');
            } 
            else {
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    console.log(row.join(", "));
                }
            }
        
    });

    let body = {
        values: [
            ["Tom","Dick","Harry"],
            ["Sally","Tina","Kathy"]
        ]
    };

    sheets.spreadsheets.values.append({
        auth: client,
        spreadsheetId: '1lCYucQH7A_wmlu2X_-Oh8PUKbF6Tupkj6SRpz3E9xl8',
        range: 'Sheet1!A2:C',
        valueInputOption: "USER_ENTERED",
        resource: body
    }, function(err, result) {
    if(err) {
        console.log(err);
    } else {
        console.log('%d cells appended.', result.updates.updatedCells);
    }
    });



//   const url = 'https://sheets.googleapis.com/v4/spreadsheets/1lCYucQH7A_wmlu2X_-Oh8PUKbF6Tupkj6SRpz3E9xl8';
//   const res = await client.request({url});
//   console.log(res.data);
}

main().catch(console.error);




// const {OAuth2Client} = require('google-auth-library');
// const http = require('http');
// const url = require('url');
// const querystring = require('querystring');
// const opn = require('opn');

// const keys = require('./client_secret.json');



// /**
//  * Start by acquiring a pre-authenticated oAuth2 client.
//  */
// async function main() {
//   try {
//     const oAuth2Client = await getAuthenticatedClient();
//     // Make a simple request to the Google Plus API using our pre-authenticated client. The `request()` method
//     // takes an AxiosRequestConfig object.  Visit https://github.com/axios/axios#request-config.
//     const url = 'https://www.googleapis.com/plus/v1/people?query=pizza';
//     const res = await oAuth2Client.request({url})
//     console.log(res.data);
//   } catch (e) {
//     console.error(e);
//   }
//   process.exit();
// }

// /**
//  * Create a new OAuth2Client, and go through the OAuth2 content
//  * workflow.  Return the full client to the callback.
//  */
// function getAuthenticatedClient() {
//   return new Promise((resolve, reject) => {
//     // create an oAuth client to authorize the API call.  Secrets are kept in a `keys.json` file,
//     // which should be downloaded from the Google Developers Console.
//     const oAuth2Client = new OAuth2Client(
//       keys.web.client_id,
//       keys.web.client_secret,
//       keys.web.redirect_uris[0]
//     );

//     // Generate the url that will be used for the consent dialog.
//     const authorizeUrl = oAuth2Client.generateAuthUrl({
//       access_type: 'offline',
//       scope: 'https://www.googleapis.com/auth/plus.me'
//     });

//     // Open an http server to accept the oauth callback. In this simple example, the
//     // only request to our webserver is to /oauth2callback?code=<code>
//     const server = http.createServer(async (req, res) => {
//       if (req.url.indexOf('/oauth2callback') > -1) {
//         // acquire the code from the querystring, and close the web server.
//         const qs = querystring.parse(url.parse(req.url).query);
//         console.log(`Code is ${qs.code}`);
//         res.end('Authentication successful! Please return to the console.');
//         server.close();

//         // Now that we have the code, use that to acquire tokens.
//         const r = await oAuth2Client.getToken(qs.code)
//         // Make sure to set the credentials on the OAuth2 client.
//         oAuth2Client.setCredentials(r.tokens);
//         console.info('Tokens acquired.');
//         resolve(oAuth2Client);
//       }
//     }).listen(3000, () => {
//       // open the browser to the authorize url to start the workflow
//       opn(authorizeUrl);
//     });
//   });
// }

//main();