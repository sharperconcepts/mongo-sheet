"use strict";

var assert = require('assert');
var {MongoData} = require('../src/lib/MongoData');

describe('Mongo Data', function(){
    this.timeout(7000);
    let m = new MongoData('mongodb://127.0.0.1:27017','Sheets101')


    it('Open and Close Database', (done) => {
        m.open().then(
            (client) => {
                client.close();
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    })

    it('Save Records', (done) => {
        let data = [
            {first:"John",last:"Smith",age:66},
            {first:"Alice",last:"Walker",age:55}
        ]

        m.saveRecords("UnitTest", data).then(
            (result) => {
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    })

    it('Retreive Records', (done) => {
        let query = {}

        m.retreiveRecords("UnitTest", query).then(
            (result) => {
                assert.notEqual(result.data.length, 0, "No data returned")
                done();
            }
        ).catch(
            (err) => {
                assert.fail(err);
                done();
            }
        )
    })

})