"use strict";

const {JWT} = require('google-auth-library');
var {google} = require("googleapis");
var ColumnName = require('./ColumnName');

class GoogleSheets {

    constructor(keys, sheetId){
        this.client = new JWT(
            keys.client_email,
            null,
            keys.private_key,
            ['https://www.googleapis.com/auth/spreadsheets'],
        );
        
        this.sheetId = sheetId;
        this.sheets = google.sheets({version: 'v4'});
    }

    authorize(){
        return this.client.authorize();
    }

    pullSheetHeader(sheet){
        return new Promise((resolve, reject) => {
            const rowRange = sheet + '!1:1';
            this.sheets.spreadsheets.values.get({
                auth: this.client,
                spreadsheetId: this.sheetId,
                range: rowRange,
            }, 
            (err, response) => {
                if (err) {
                    reject( err );
                } else {
                    var rows = response.data.values;
                    if (rows.length === 0) {
                        reject( "No data returned." );
                    } 
                    else {
                        resolve( {
                            status: "OK",
                            error: "Data returned.",
                            data: rows[0]
                        } );
                    }
                }     
            });
        });
    }

    pullSheetData(sheet, width, start = "A2"){
        return new Promise((resolve, reject) => {
            const rowRange = sheet + '!' + start + ':' + ColumnName.intToExcelCol(width);
            this.sheets.spreadsheets.values.get({
                auth: this.client,
                spreadsheetId: this.sheetId,
                range: rowRange,
            }, 
            (err, response) => {
                if (err) {
                    reject( err );
                } else {
                    var rows = response.data.values;
                    if (rows.length === 0) {
                        reject( "No data returned." );
                    } 
                    else {
                        resolve( {
                            status: "OK",
                            error: "Data returned.",
                            data: rows
                        } );
                    }
                }     
            });
        });
    }

    insertSheetData(sheet, data, start = "A2"){
        return new Promise((resolve, reject) => {
            const rowRange = sheet + '!' + start + ':' + ColumnName.intToExcelCol(data[0].length);
            this.sheets.spreadsheets.values.append({
                auth: this.client,
                spreadsheetId: this.sheetId,
                range: rowRange,
                valueInputOption: "USER_ENTERED",
                resource: { values:data },
            }, 
            (err, response) => {
                if (err) {
                    reject( err );
                } else {
                    var rows = response.data.updates.updatedRows;
                    if (rows.length === 0) {
                        reject( "No data returned." );
                    } 
                    else {
                        resolve( {
                            status: "OK",
                            error: "Data updated.",
                            data: rows
                        } );
                    }
                }     
            });
        });
    }

    updateSheetData(sheet, range, data){
        return new Promise((resolve, reject) => {
            const rowRange = sheet + '!' + range;
            this.sheets.spreadsheets.values.update({
                auth: this.client,
                spreadsheetId: this.sheetId,
                range: rowRange,
                valueInputOption: "USER_ENTERED",
                resource: { values:data },
            }, 
            (err, response) => {
                if (err) {
                    reject( err );
                } else {
                    var rows = response.data.updatedRows;
                    if (rows.length === 0) {
                        reject( "No data returned." );
                    } 
                    else {
                        resolve( {
                            status: "OK",
                            error: "Data updated.",
                            data: rows
                        } );
                    }
                }     
            });
        });
    }
    
    GetSpreadSheetInfo(){
        return new Promise((resolve, reject) => {
            this.sheets.spreadsheets.get({
                auth: this.client,
                spreadsheetId: this.sheetId,
                ranges: [],
            }, 
            (err, response) => {
                if (err) {
                    reject( err );
                } else {
                    resolve( {
                        status: "OK",
                        error: "Spreadsheet retrieved.",
                        data: response
                    } );
                }     
            });
        });
    }

    InsertColumn(sheet, loc = 0){

        let requests = [];
        requests.push({
            insertDimension: {
                range: {
                    sheetId: sheet,
                    dimension: "COLUMNS",
                    startIndex: loc,
                    endIndex: loc + 1
                },
                inheritFromBefore: false
            }
        });

        return new Promise((resolve, reject) => {
            this.sheets.spreadsheets.batchUpdate({
                auth: this.client,
                spreadsheetId: this.sheetId,
                resource: {requests}
            },
            (err, response) => {
                if (err) {
                    reject( err );
                } else {
                    resolve( {
                        status: "OK",
                        error: "Data updated.",
                        data: response
                    } );
                }    
            });


        });  
    }

}

exports.GoogleSheets = GoogleSheets;